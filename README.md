![](./timicard.png)

# mysqlata

BASH files for managing a MySQL DB.

The benefit of using mysqlata over standard mysql is that when you are working
on a single project it helps to have COMMAND history where the thing  which
changes the most, comes last. The thing which needs changing most when  dealing
with DUMP files is the DBNAME. The user, host and HOST mostly stay the same
(on any one machine)  when you executing mysql COMMANDs.

Another benefit is that mysqlata decides when to use mysqldump or mysql.

Another benefit is that mysqlata has a simple bash friendly method of giving
your mysql DUMPs friends distinshing names.  We could have just used a
timestamp but a friendly name makes it easy to restore. We call this unique name
the "MYSQLATA".

## Install

```
cd path/to/your/path
git clone git@github.com:timitee/mysqlata.git
```

Add `path/to/your/path/mysqlata` to your PATH.

## Overview

* mysqlata assumes you are working in any project/app folder.
  * You can work in any folder, but once you have run a mysqlata COMMAND,
    any future access to any DUMP or SCHEMA files have to be executed in
    the same folder, as mysqlata accesses only the DUMPs in the folder where
    the COMMAND was executed.
* In you current folder, mysqlata creates a `.mysqlata` folder ...
  * Then a folder for each [DBNAME].
  * Your DUMPs are stored in these folders.
  * No two DUMPs should have the same MYSQLATA.
* mysqlata creates a `.mysqlemas` folder.
  * Your SCHEMAS are all stored in this folder.
  * No two SCHEMAS should have the same DBNAME+MYSQLATA.

We recommend adding `.mysqlata` to your `.gitignore` file. Make any backups of
these files independantly. Anything in `.mysqlemas` contains no data, only the
entire structure of the database, including any functions and stored procedures,
but not views. You can have git add the `.mysqlemas` folder as it will contain a
mysqlated history of your app's DB structure... even if it is being handled by
an MVC, this can be useful.

## Usage

```
cd path/to/my/project/app
mysqlata [DBUSER] [DBHOST] [DBNAME] [COMMAND] [MYSQLATA] [arg1 OPTIONAL] [arg2 OPTIONAL]
```
* [DBUSER] an active database user which you have permission to use.
* [DBHOST] the HOST, usually localhost.
* [DBNAME] the database name you are working with.
* [COMMAND] one of the COMMANDs below.
* [MYSQLATA] usually a unique name or version ID for a DUMP file
  * Also the parameter of `adduser` COMMAND (see below).
  * Apart from adduser, I conventionally make my MYSQLATAs uppercase to avoid
    confusion with the MySQL elements - like the database name. MYSQLATAs are
    for keeping lots of versions of backups.
* [arg1]  
  * new DBNAME for the `clone` COMMAND (see below)
  * password for `adduser` COMMAND (see below)
  * user@host:/path/to/app for the `send` and `receive` COMMANDs (see below)
* [arg2]  a new MYSQLATA for new DBNAME for the `clone` COMMAND (see below)

## Examples

```
cd path/to/my/project/app
mysqlata clubuser localhost chococlubDB dump 16MAR18
mysqlata clubuser localhost chococlubDB dump 22MAR18
mysqlata clubuser localhost chococlubDB dump 25MAR18
mysqlata clubuser localhost chococlubDB restore 22MAR18
mysqlata clubuser localhost chococlubDB clone 25MAR18 zeezeetopDB ZEEZEEINAUGURATION
mysqlata clubuser localhost zeezeetopDB create
mysqlata clubuser localhost zeezeetopDB restore ZEEZEEINAUGURATION
```

## MySql or MySqlDump COMMANDs

* adduser:  Add a new user.
  * [MYSQLATA] = new username.
  * eg: ```mysqlata clubuser localhost chococlubDB adduser ch0c0h0|icPa$$```

* clone:    Copy a DUMP file and rename its DBNAME and/or its MYSQLATA.
  * [arg1] = new DBNAME.
  * [arg2] = new MYSQLATA.
  * eg: ```mysqlata clubuser localhost chococlubDB dump 16MAR18```
  * then: ```mysqlata clubuser localhost chococlubDB clone zeezeetopDB ZEEZEEINAUGURATION```
  * then: ```mysqlata clubuser localhost zeezeetopDB restore ZEEZEEINAUGURATION```

* create:   Creates the database. Used by mysqlata internally, but convenient.
  * eg: ```mysqlata clubuser localhost chococlubDB create```

* drop:     Drops a database.
  * eg: ```mysqlata clubuser localhost chococlubDB drop```

* dump:     Makes a backup or DUMP of the database which is zipped up.
  * dumpfile is stored in the folder: .mysqlata/[DBNAME]/[DBNAME].[MYSQLATA].sql.gz
  * eg: ```mysqlata clubuser localhost chococlubDB dump 16MAR18```

* login:    Login to work on the MySql COMMAND line directly.
  * eg: ```mysqlata clubuser localhost chococlubDB login```

* restore:  Restore the database from a DUMP file.
  * Useful in conjungtion with clone to view a backup file without wiping your live data.
  * eg: ```mysqlata clubuser localhost chococlubDB dump 16MAR18```
  * then: ```mysqlata clubuser localhost chococlubDB clone 16MAR18 zeezeetopDB ZEEZEEINAUGURATION```
  * then: ```mysqlata clubuser localhost zeezeetopDB restore ZEEZEEINAUGURATION ```

* blank:    Rebuild a version of the database from SCHEMA with no data.
  * Will remove data! Can edit though.
  * Useful in conjungtion with reverse to wipe your live/cloned data.
  * eg: ```mysqlata clubuser localhost chococlubDB dump 16MAR18```
  * then: ```mysqlata clubuser localhost chococlubDB clone 16MAR18 zeezeetopDB ZEEZEEINAUGURATION```
  * then: ```mysqlata clubuser localhost zeezeetopDB restore ZEEZEEINAUGURATION ```
  * then: ```mysqlata clubuser localhost zeezeetopDB reverse ZEEZEEINAUGURATION ```
  * then: ```mysqlata clubuser localhost zeezeetopDB blank ZEEZEEINAUGURATION ```

* reverse:  Reverse engineer the data into a SCHEMA file ...
  * Which is basically a DUMP file with no data stored in a different folder.
  * eg: ```mysqlata clubuser localhost chococlubDB reverse 16MAR18```

## Other COMMANDs

* setup:    Setup creates the folders mysqlata needs.
  * mysqlata creates all the folders it needs when it needs them but ...
  * ... if you are transfering from another HOST, you'll need the folders first.
  * eg: ```mysqlata clubuser localhost chococlubDB setup 16MAR18```
  * then: ```scp cuser@choco.com:/home/cuser/repo/chocoapplive/.mysqlata/chococlubDB/chococlubDB.16MAR18.sql.gz ./.mysqlata/chococlubDB/```

* receive:  Convenient SCP to receive a MYSQLATED DUMP from another HOST.
  * [arg1] = Remote Path to app folder.
    * meaning a folder where you used the mysqlata COMMAND to create a dump ...
    * but not the .mysqlata sub path.
  * eg: ```ssh cuser@choco.com  #e.g. remote into another HOST with mysqlata installed```
  * then: ```cm /home/cuser/repo/chocoapplive/  # select a folder ```
  * then: ```mysqlata cuser 127.0.0.1 chococlubDB dump 16MAR18  # create a DUMP ```
  * then: ```exit  # exit the remote session```
  * then: ```# receive that DUMP to your local machine:```
  * then: ```mysqlata clubuser localhost chococlubDB receive 16MAR18 cuser@choco.com:/home/cuser/repo/chocoapplive```

* send:     Convenient SCP to send a MYSQLATED DUMP to another HOST.
  * [arg1] = Remote Path to app folder.
  * eg: ```mysqlata clubuser localhost chococlubDB dump 16MAR18  # create a DUMP ```
  * then: ```# send that DUMP to a remote machine - assumes it has those folders```
  * then: ```mysqlata clubuser localhost chococlubDB send 16MAR18 cuser@choco.com:/home/cuser/repo/chocoapplive```

* sed:      Search and replace the DUMP file to make it user agnostic and normalize some character codings.
* eg: ```mysqlata clubuser localhost chococlubDB dump 16MAR18```
* then: ```mysqlata clubuser localhost chococlubDB restore 16MAR18```
* Permission errors!?
* Check sed search and replace list for - and add 'permission error user' if missing.
* then: ```mysqlata clubuser localhost chococlubDB sed 16MAR18```
* then: ```mysqlata clubuser localhost chococlubDB restore 16MAR18```
* Success! 'chococlubDB.16MAR18' fixed permanantly!

* unzip:    Unzip a DUMP file. Used by mysqlata internally, but convenient.
  * It's useful if you want to open a DUMP file into a text editor
  * eg: ```mysqlata clubuser localhost chococlubDB unzip 16MAR18```

* zip:      Zip a DUMP file. Used by mysqlata internally, but convenient.
  * Put it back the mysqlata way after you've used it.
  * eg: ```mysqlata clubuser localhost chococlubDB zip 16MAR18```

## Tests

Add "timemachine" database and "timetraveller" as a user to your mysql HOST:

```
mysqlata standardDBUSER localhost timemachine create
mysqlata standardDBUSER localhost timemachine adduser timetraveller
```

Then run the tests:

```
mysqlata timetraveller localhost timemachine test TODAY travelto TOMORROW
```


![](./appicon.png)